# Spark Filtering By Value #

Input:

    [root@sandbox ~]# cat test.csv 
    A,1
    B,2
    C,2
    D,3
    E,4
    F,5
    [root@sandbox ~]# 

Spark code:

    %spark2

    case class fileRow (key: String, value: String);

    var fileRDD = sc.textFile("/tests/test.csv");

    var kvDF = fileRDD.map(line=>new fileRow(line.split(",")(0), line.split(",")(1))).toDF();

    kvDF.createOrReplaceTempView("keyValueT");

    sqlContext.sql("select * from keyValueT where value > 3").show();

Zeppelin output:

    defined class fileRow
    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/test.csv MapPartitionsRDD[32] at textFile at <console>:27
    kvDF: org.apache.spark.sql.DataFrame = [key: string, value: string]
    +---+-----+
    |key|value|
    +---+-----+
    |  E|    4|
    |  F|    5|
    +---+-----+


